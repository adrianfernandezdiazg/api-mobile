Hi everyone!

Here is a little nodejs proyect made to solve a 
technical test.

How to run the proyect ->

    - npm install.

    If u want to use production db : npm run pro (need to have configured your config/pro.env file)

    If u want to use production db : npm run dev (need to have configured your config/dev.env and your local mongodb)

    If u want to test: npm run test (need to have configured your config/test.env).


Proyect summary:

As dependencies, we can find:
    main dependencies:

    bcryptjs -> hash passwords.
    express -> framework app based.
    jest -> testing.
    jsonwebtoken -> generate tokens and validate them.
    mongoose -> ORM with MongoDB.
    supertest -> Generate the test environment.

    dev depencies:
    
    env-cmd -> read env variables.
    nodemon -> automatically restarting app.

As scripts, we can find:
    dev -> instantiates the proyect with a local mongo (need to prepare your machine with mongodb)
    pro -> instantiates the proyect with production mongodb, using a guest user credentials.
    test ->instantiates the proyect with test production mongodb, using a guest user credentials.

Little folders guide:
    - /src -> 
        - /controllers -> app controllers. At the moment, user controllers.
        - /db -> conexion with db.
        - /middlwares -> logic used before route controllers. Currently there are 2 middlwares: 
            1. auth.js -> Checks the jwt hosted into the authorization header, and checks if it is a valid token.
            2. logEpAndDate -> logs date and current api endpoint.
        - /models -> here is where schemas and models are allowed, as well as schema hooks, model logic, etc. By the moment, there is just the User model.
        - /routes -> here is a static literals object with the main routes.
        - /routers -> route handlers, connecting client with controllers. Also setting middlewares.
        - /services -> some app services. Currenlty there is just an login service, that checks if user is already registered, and returns a token.

    - app.js -> Where the app is turned on, settings routers and express configuration.
    - index.js -> Entry point.
    - /test -> Tests 
        /fixtures/db -> The tests fixtures configuration. 
    - package.json & packege-lock.json -> dependencies and proyect data.

    ***Hidden -> 
    /config -> proyect environment variables. At the moment, there are 3 available configurations: dev, pro and test.
    /node_modules -> dependencies.
               
