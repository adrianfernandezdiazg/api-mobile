const express = require('express')
require('./db/mongoose')
const app = express()
const userRouter = require('./routers/user')

// CORS Configuration
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  res.header('Allow', 'GET, POST, OPTIONS, PATCH, PUT, DELETE')
  next()
})

app.use(express.json())
app.use(userRouter)

module.exports = app
