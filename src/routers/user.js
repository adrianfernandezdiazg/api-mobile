const express = require('express')
const UserController = require('../controllers/user')
const router = express.Router()
const userRoutes = require('../routes/user')

// middlewares

const auth = require('../middlewares/auth')
const logEpAndDate = require('../middlewares/logEpAndDate')

// routers
router.post(userRoutes.core, logEpAndDate, UserController.create)
router.get(userRoutes.core, auth, logEpAndDate, UserController.getAll)
router.patch(`${userRoutes.core}/:id`, logEpAndDate, auth, UserController.update)
router.delete(`${userRoutes.core}/:id`, logEpAndDate, auth, UserController.delete)
router.post('/login', logEpAndDate, UserController.login)

module.exports = router
