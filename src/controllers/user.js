const User = require('../models/user')
const loginService = require('../services/login.service')

const controller = {
  create: async (req, res) => {
    const { username, password } = req.body
    const user = new User({ username, password })
    try {
      await user.save()
      const { username } = user
      res.status(201).send({ username })
    } catch (error) {
      res.status(400).send({ error })
    }
  },

  getAll: async (req, res) => {
    try {
      const users = await User.find()
      res.status(200).send({ users })
    } catch (error) {
      res.status(400).send({ error })
    }
  },

  update: async (req, res) => {
    const validKeysToUpdate = ['username', 'password']
    const updatePayload = {}

    Object.keys(req.body).forEach(key => {
      if (validKeysToUpdate.includes(key)) {
        updatePayload[key] = req.body[key]
      }
    })
    if (!Object.keys(updatePayload).length) {
      return res.status(400).send({ error: 'Invalid update' })
    }
    try {
      const userToUpdate = await User.findById(req.params.id)
      if (!userToUpdate) {
        return res.status(404).send({ error: `User with id: ${req.params.id} not found` })
      }
      const updatedUser = new User(Object.assign(userToUpdate, updatePayload))
      await updatedUser.save()
      res.send(updatedUser)
    } catch (error) {
      res.status(400).send({ error })
    }
  },

  delete: async (req, res) => {
    const userToDelete = await User.findById(req.params.id)
    if (!userToDelete) {
      return res.status(404).send({ error: `User with id: ${req.params.id} not found` })
    }

    try {
      await userToDelete.delete()
      res.send({ message: 'User correctly deleted', user: userToDelete })
    } catch (error) {
      res.status(400).send({ error })
    }
  },

  login: async (req, res) => {
    try {
      const token = await loginService(req.body.username, req.body.password)
      res.send({ token })
    } catch (error) {
      res.status(400).send({ error: error.message })
    }
  }
}

module.exports = controller
