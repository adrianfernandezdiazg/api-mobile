
const logEpAndDate = async (req, res, next) => {
  console.log(`endpoint: ${req.path}, date: ${new Date().toLocaleString()}`)
  next()
}

module.exports = logEpAndDate
