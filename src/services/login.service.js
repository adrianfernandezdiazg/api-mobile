const User = require('../models/user')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

// Token lasts 1 hour
const jwtLifeTime = 60 * 60

const loginService = async (username, password) => {
  const user = await User.findOne({ username })

  if (!user) {
    throw new Error(`User with username: ${username} was not found`)
  }

  const isMatch = await bcrypt.compare(password, user.password)
  if (!isMatch) {
    throw new Error('Password doesnt match')
  }
  const token = await generateAuthToken(user)
  if (!token) {
    throw new Error('Error creating token')
  }
  user.tokens = [...user.tokens, { token }]
  await user.save()
  return token
}

const generateAuthToken = (user) => {
  return jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET, { expiresIn: jwtLifeTime })
}
module.exports = loginService
