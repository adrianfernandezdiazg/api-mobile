const request = require('supertest')
const app = require('../src/app')
const User = require('../src/models/user')
const jwt = require('jsonwebtoken')
const {
  setupDatabase,
  registeredUserOne,
  notRegisteredUserOne,
  notRegisteredUserId,
  invalidUser,
  validToken,
  loggedUserId
} = require('./fixtures/db')

beforeEach(setupDatabase)

// CRUD
// create -> POST baseurl/users

test('Should signup a new user with valid payload', async () => {
  const usersBeforeCreating = await User.find()
  await request(app).post('/users').send(
    notRegisteredUserOne
  ).expect(201)
  const usersAfterCreating = await User.find()
  expect(usersAfterCreating.length).not.toBe(usersBeforeCreating.length)
})

test('Should not signup a new user with invalid payload', async () => {
  const usersBeforeCreating = await User.find()
  await request(app).post('/users').send(
    invalidUser
  ).expect(400)
  const usersAfterCreating = await User.find()
  expect(usersAfterCreating.length).toBe(usersBeforeCreating.length)
})

test('Should not signup a user that is already registered', async () => {
  const usersBeforeCreating = await User.find()
  await request(app).post('/users').send(
    registeredUserOne
  ).expect(400)
  const usersAfterCreating = await User.find()
  expect(usersAfterCreating.length).toBe(usersBeforeCreating.length)
})

// getAll -> GET baseurl/users

test('Should get unauthorized if user is not logged in', async () => {
  await request(app).get('/users').send(
    registeredUserOne
  ).expect(401)
})

test('Should get users list if user is logged in', async () => {
  await request(app).get('/users')
    .set('Authorization', `Bearer ${validToken}`)
    .expect(200)
})

test('List should change if user is added', async () => {
  const usersBeforeCreating = await User.find()
  await request(app).post('/users').send(notRegisteredUserOne)
    .set('Authorization', `Bearer ${validToken}`)
    .expect(201)
  const usersAfterCreating = await User.find()
  expect(usersAfterCreating.length).not.toBe(usersBeforeCreating.length)
})

// delete -> DELETE baseurl/users/:id

test('Should delete a user if logged and user exists in db', async () => {
  const usersBeforeDeleting = await User.find()
  await request(app).delete(`/users/${loggedUserId}`)
    .set('Authorization', `Bearer ${validToken}`)
    .expect(200)
  const usersAfterDeleting = await User.find()
  expect(usersBeforeDeleting.length).not.toBe(usersAfterDeleting.length)
})

test('Should not delete a user if not logged and user exists in db', async () => {
  const usersBeforeDeleting = await User.find()
  await request(app).delete(`/users/${loggedUserId}`)
  const usersAfterDeleting = await User.find()
  expect(usersAfterDeleting.length).toBe(usersBeforeDeleting.length)
})

test('Should not delete a user if user does not exist in db', async () => {
  const usersBeforeDeleting = await User.find()
  await request(app).delete(`/users/${notRegisteredUserId}`)
    .set('Authorization', `Bearer ${validToken}`)
    .expect(404)
  const usersAfterDeleting = await User.find()
  expect(usersBeforeDeleting.length).toBe(usersAfterDeleting.length)
})

// login -> POST baseurl/login

test('Should login if user is registered', async () => {
  await request(app).post('/login').send(
    registeredUserOne
  ).expect(200)
})

test('Should not login if user is not registered', async () => {
  await request(app).post('/login').send(
    notRegisteredUserOne
  ).expect(400)
})

test('Login should return a valid token', async () => {
  const response = await request(app).post('/login').send(
    registeredUserOne
  ).expect(200)

  const verification = jwt.verify(response.body.token, process.env.JWT_SECRET)
  expect(Object.keys(verification)).toContain('_id')
})

// update -> PATCH baseurl/users/:id, payload

test('Should not update if not logged', async () => {
  await request(app).patch(`/users/${loggedUserId}`).send(
    notRegisteredUserOne
  ).expect(401)
})

test('Should update if logged in and correct payload', async () => {
  const response = await request(app).patch(`/users/${loggedUserId}`)
    .send(
      notRegisteredUserOne
    )
    .set('Authorization', `Bearer ${validToken}`)
    .expect(200)
  expect(response.body.username).toBe(notRegisteredUserOne.username)
})

test('Should not update if logged and wrong payload', async () => {
  const response = await request(app).patch(`/users/${loggedUserId}`)
    .send(
      invalidUser
    )
    .set('Authorization', `Bearer ${validToken}`)
    .expect(400)
  expect(response.body.username).not.toBe(invalidUser.username)
})

test('Should not update if user does not exist on db', async () => {
  const response = await request(app).patch(`/users/${notRegisteredUserId}`)
    .send(
      invalidUser
    )
    .set('Authorization', `Bearer ${validToken}`)
    .expect(404)
  expect(response.body.username).not.toBe(invalidUser.username)
})
