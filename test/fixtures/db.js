const mongoose = require('mongoose')
const User = require('../../src/models/user')
const jwt = require('jsonwebtoken')

const registeredUserOne = {
  username: 'registeredOneName',
  password: 'registeredOnePw'
}

const registeredUserTwo = {
  username: 'registeredTwoName',
  password: 'registeredTwoPw'
}

const notRegisteredUserOne = {
  username: 'notRegisteredOneName',
  password: 'notRegisteredOnePw'
}

const invalidUser = {
  username: 'randomUserName',
  password: 'password123'
}

const loggedUserId = new mongoose.Types.ObjectId()
const loggedUser = {
  _id: loggedUserId,
  username: 'loggedUserName',
  password: 'loggedUserPw',
  tokens: [{
    token: jwt.sign({ _id: loggedUserId }, process.env.JWT_SECRET)
  }]
}

const validToken = loggedUser.tokens[0].token

const notRegisteredUserId = new mongoose.Types.ObjectId()

const setupDatabase = async () => {
  await User.deleteMany()
  await new User(registeredUserOne).save()
  await new User(registeredUserTwo).save()
  await new User(loggedUser).save()
}

module.exports = {
  setupDatabase,
  registeredUserOne,
  notRegisteredUserOne,
  invalidUser,
  validToken,
  loggedUserId,
  notRegisteredUserId
}
